//
//  Validator.m
//  charsets
//
//  Created by Abizer Nasir on 09/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import "Validator.h"

@implementation Validator

+ (BOOL)isValidString:(NSString *)string {
    // I see you also have a length validation to do as well, filter for that first

    NSUInteger inputLength = [string length];
    if (!(inputLength > 0 && inputLength <= 30)) {
        return NO; // bail and return NO
    }

    // Create a static comparison set for performance
    // Basically - specify the character set you want and then invert the set to get a set of characters you don't want.
    static NSCharacterSet *unwantedCharacters = nil;
    if (!unwantedCharacters) {
        unwantedCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    }

    NSRange rangeOfUnwantedCharacters = [string rangeOfCharacterFromSet:unwantedCharacters];

    // If any characters are found from the invalid character set, they will have a valid range.
    if (rangeOfUnwantedCharacters.location == NSNotFound) {
        return YES;
    } else {
        return NO;
    }
}

@end
