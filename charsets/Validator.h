//
//  Validator.h
//  charsets
//
//  Created by Abizer Nasir on 09/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validator : NSObject

+ (BOOL)isValidString:(NSString *)string;

@end
