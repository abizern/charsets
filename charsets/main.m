//
//  main.m
//  charsets
//
//  Created by Abizer Nasir on 09/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Validator.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSString *goodString = @"abcdefghijklmnopqrstuvwxyz0123456789";
        NSString *badString = @"[this & this are wrong]";
        NSString *tooLongString = @"abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789";

        NSLog(@"Good String is valid: %@", [Validator isValidString:goodString] ? @"YES" : @"NO");
        NSLog(@"Bad String is valid: %@", [Validator isValidString:badString] ? @"YES" : @"NO");
        NSLog(@"Too Long String is valid: %@", [Validator isValidString:tooLongString] ? @"YES" : @"NO");
        
    }
    return 0;
}


